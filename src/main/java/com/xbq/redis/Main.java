package com.xbq.redis;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

import redis.clients.jedis.Jedis;

/**
 * 模拟并发环境下 获取锁
 * @author xbq
 */
public class Main {

	public static void main(String[] args) {

		// 定义线程池
		ExecutorService service = Executors.newCachedThreadPool();
		
		// 只能有10个线程同时访问，用来模拟并发
		final Semaphore semaphore = new Semaphore(10);
		
		// 模拟20个客户端访问 
		for (int i = 0; i < 20; i++) {
			Runnable runnable = new Runnable() {
				String lockKey = "TestLock33";
				@Override
				public void run() {
					try {
						// 获取许可 
						semaphore.acquire();
						// 获取jedis实例
						Jedis jedis = new JedisUtil().common();

						RedisLock redisLock = new RedisLock(jedis, lockKey, 10000);
						if(redisLock.lock()){    // 获取到了锁，然后进行 业务处理
							// 业务代码
		                    Thread.sleep(3000);
						}
	                    // 释放锁
	                    redisLock.unLock();
	                    // 访问完后，释放 ，如果屏蔽下面的语句，则在控制台只能打印5条记录，之后线程一直阻塞
						semaphore.release();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			};
			// 执行线程
			service.execute(runnable);
		}
		// 退出线程池 
		service.shutdown();
	}
}
