package com.xbq.lock;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
/**
 * Jedis工具类
 * @author xbq
 * @created：2017-4-19
 */
public class JedisUtil {

	private static JedisPool pool;
	private static String URL = "192.168.242.130";
	private static int PORT = 6379;
//	private static String PASSWORD = "xbq123";
	
	// ThreadLocal，给每个线程 都弄一份 自己的资源
	private final static ThreadLocal<JedisPool> threadPool = new ThreadLocal<JedisPool>();
	private final static ThreadLocal<Jedis> threadJedis = new ThreadLocal<Jedis>();
	
	private final static int MAX_TOTAL = 100;   // 最大分配实例
	private final static int MAX_IDLE = 50;     // 最大空闲数
	private final static int MAX_WAIT_MILLIS = -1; // 最大等待数
	
	/**
	 * 获取 jedis池
	 * @return
	 */
	public static JedisPool getPool(){
		JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
		// 控制一个pool可分配多少个jedis实例，通过pool.getResource()来获取，如果赋值为-1，则表示不限制；
		// 如果pool已经分配了maxActive个jedis实例，则此时pool的状态为exhausted(耗尽)
		jedisPoolConfig.setMaxTotal(MAX_TOTAL);
		// 控制一个pool最多有多少个状态为idle(空闲的)的jedis实例
		jedisPoolConfig.setMaxIdle(MAX_IDLE);
		// 表示当borrow(引入)一个jedis实例时，最大的等待时间，如果超过等待时间，则直接抛出JedisConnectionException
		jedisPoolConfig.setMaxWaitMillis(MAX_WAIT_MILLIS);
		
		final int timeout = 60 * 1000;
		pool = new JedisPool(jedisPoolConfig, URL, PORT, timeout);
		
		return pool;
	}
	
	/**
	 * 在jedis池中 获取 jedis
	 * @return
	 */
	public static Jedis common(){
		// 从 threadPool中取出 jedis连接池
		pool = threadPool.get();
		// 为空，则重新产生 jedis连接池
		if(pool == null){
			pool = getPool();
			// 将jedis连接池维护到threadPool中
			threadPool.set(pool);
		}
		// 在threadJedis中获取jedis实例
		Jedis jedis = threadJedis.get();
		// 为空，则在jedis连接池中取出一个
		if(jedis == null){
			jedis = pool.getResource();
			// 验证密码
//			jedis.auth(PASSWORD);
			// 将jedis实例维护到threadJedis中
			threadJedis.set(jedis);
		}
		return jedis;
	}
	
	/**
	 * 释放资源
	 */
	public static void closeAll(){
		Jedis jedis = threadJedis.get();
		if(jedis != null){
			threadJedis.set(null);
			JedisPool pool = threadPool.get();
			if(pool != null){
				// 释放连接，归还给连接池
				pool.returnResource(jedis);
			}
		}
	}
}
